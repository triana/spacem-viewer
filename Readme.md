# Image for https://spacem-viewer.shiny.embl.de/

## Building image
sudo docker build -t git.embl.de:4567/triana/spacem-viewer/shiny:3.6.1-SpaceM-viewer .
## Login
docker login git.embl.de:4567

## Push it 
sudo docker push git.embl.de:4567/triana/spacem-viewer/shiny:3.6.1-SpaceM-viewer .

##To run
sudo docker run --rm -p 3838:3838 DockerImage