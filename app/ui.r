#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  # Application title
  titlePanel("SpaceM Viewer App"),
  tags$strong("Citation:"),
  tags$em("Rappez, L., Stadler, M., Triana, S. et al. SpaceM reveals metabolic states of single cells. Nat Methods 18, 799–805 (2021). DOI:"),tags$a("10.1038/s41592-021-01198-0", href = "https://doi.org/10.1038/s41592-021-01198-0 "),
  tags$p(tags$strong("Contact:"), tags$a("Sergio Triana,", href = "mailto:triana@embl.de"), 
         " at EMBL Heidelberg"),
  tags$p("Please use Google Chrome when running the app and wait a couple of minutes while the data loads, in the meantime you can check the following tutorial", 
         tags$a("video.", href="https://vimeo.com/578681345", target="_blank")),
  fluidRow(
    column(5,uiOutput('Select_Experiment'),
           ),
    column(4,uiOutput('Select_samples')),
    column(3,uiOutput('slide_percentage'))
  ),
  tabsetPanel(type = "tabs",
              tabPanel("Visualisation", 
                       fluidRow(width = 9, 
                                textOutput("dataset")
                       ),     
                       fluidRow(
                         column(width=3, radioButtons("tsne.type", label="What to highlight on the UMAP", selected = "Samples", choices = c("Samples","Cell type","Metabolites/Features"))),
                         column(width=5,uiOutput('columns'))
                       ),
                       fluidRow(
                         column(width= 5, tags$strong("UMAP"), tags$br(),
                                tags$i("This UMAP was calculated using the data obatained from SpaceM"),verbatimTextOutput("hover_info")),
                         column(width=3, uiOutput('Second_plot')),
                         column(width=2,uiOutput('label')),column(width=2,uiOutput('order'))
                       ),
                       fluidRow(
                         tags$head(tags$script('
                                               var dimension = [0, 0];
                                               $(document).on("shiny:connected", function(e) {
                                               dimension[0] = window.innerWidth;
                                               dimension[1] = window.innerHeight;
                                               Shiny.onInputChange("dimension", dimension);
                                               });
                                               $(window).resize(function(e) {
                                               dimension[0] = window.innerWidth;
                                               dimension[1] = window.innerHeight;
                                               Shiny.onInputChange("dimension", dimension);
                                               });
                                               '))
                         ,
                         #create rows / columns dynamically according to the number of plots
                         column(width=5,uiOutput('plotTsne'),style='padding:0px;',align="left"),
                         column(width=6,uiOutput('plotPie'),style='padding:0px;',align="left")
                       ),  
                       fluidRow(column(width=2, uiOutput("umap.labels")),column(width=3, uiOutput("cell.size"))),
                       #fluidRow(column(width=2,checkboxInput("label", "Show Label", FALSE)),column(width=10,align="center",plotOutput("legend.tsne",height = '300px'),style='padding:-100px;'))
                       fluidRow(column(width=10,align="center",uiOutput('plotLegend'),style='padding:-100px;'))
                       
              )
              
              ,
              tabPanel("Differential Expression",
                       fluidRow(
                         column(width=6, radioButtons("diff.type", label="What Label to use", selected = "Clusters", choices = c("Clusters","Sample"),inline=T))
                         
                       ),
                       fluidRow(
                         column(width=4,uiOutput('Cell1')),
                         column(width=4,uiOutput('Cell2'))
                         ),
                       fluidRow(
                         column(width=4,uiOutput('Cond1')),
                         column(width=4,uiOutput('Cond2')),
                         column(width=4,align="left",actionButton("calculate", "Calculate",style='padding:10px; font-size:100%'))
                         
                       ),
                       fluidRow(
                         column(width = 6, tags$strong("Volcano Plot")),
                         column(width= 4, tags$strong("Differential expression result"), tags$br())
                       ),
                       fluidRow(
                         #create rows / columns dynamically according to the number of plots
                         column(width=6,plotOutput("Voldano.pl",height="600px")),
                         column(width=4,withLoader(DTOutput("tableDE"),type='html',loader="dnaspin"))),
                       fluidRow(
                         column(width = 6, downloadButton("downloadData", "Download DE table")))
              ),
              tabPanel("Upload dataset",
                       tags$div(tags$strong("Expression matrix"), tags$br(),
                                tags$i("Please create your csv file with cells as columns and metabolites as rows,
                                       example, "),
                                downloadLink("LinkMatrix", "example matrix")),
                       fluidRow(column(width=4,fileInput("file1", "Choose the expression matrix CSV File",
                                   multiple = TRUE,
                                   accept = c("text/csv",
                                              "text/comma-separated-values,text/plain",
                                              ".csv"))),
                       column(width=2,checkboxInput("header", "Header", TRUE)),
                       column(width=3,
                         # Input: Select separator ----
                         radioButtons("sep", "Separator",
                                      choices = c(Comma = ",",
                                                  Semicolon = ";",
                                                  Tab = "\t"),
                                      selected = ",")),
                       column(width=3,
                         
                         # Input: Select quotes ----
                         radioButtons("quote", "Quote",
                                      choices = c(None = "",
                                                  "Double Quote" = '"',
                                                  "Single Quote" = "'"),
                                      selected = '"'))),
                         tags$hr(),
                       tags$div(tags$strong("Metadata"), tags$br(),
                                tags$i("Please create your metadata with cells as
                                       rows and each variable as columns, "),
                                downloadLink("LinkMeta", "example of metadata")),
                       fluidRow(column(width=4, fileInput("file2", "Choose CSV File for metadata",
                                   multiple = TRUE,
                                   accept = c("text/csv",
                                              "text/comma-separated-values,text/plain",
                                              ".csv"))),
                         # Horizontal line ----
                         column(width=2, 
                         # Input: Checkbox if file has header ----
                         checkboxInput("header2", "Header", TRUE)),
                         
                         # Input: Select separator ----
                         column(width=3,radioButtons("sep2", "Separator",
                                      choices = c(Comma = ",",
                                                  Semicolon = ";",
                                                  Tab = "\t"),
                                      selected = ",")),
                         
                         # Input: Select quotes ----
                         column(width=3,radioButtons("quote2", "Quote",
                                      choices = c(None = "",
                                                  "Double Quote" = '"',
                                                  "Single Quote" = "'"),
                                      selected = '"'))),
                        column(width=12,uiOutput('SelectColumn')),
                        fluidRow(column(width=4,uiOutput('UmapX')),column(width=4,uiOutput('UmapY')),
                        column(width=4,uiOutput('MetaSamples'))),
                        fluidRow(column(width=4,uiOutput('MetaX')),
                                 column(width=4,uiOutput('MetaY')),
                                 column(width=4,uiOutput('MetaConditions'))),
                        fluidRow(column(width=4,uiOutput('MetaClusters')),
                                 column(width=4,align="left",uiOutput('Createbutton')),
                           column(width=4,withLoader(textOutput('TextCreated'),type='html',loader="dnaspin")))
                           # Output: Data file ----
                          
                           
                         )
                       
                       
                       
              
              
  ))
)

